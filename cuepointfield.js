
var Cuepointfield = Cuepointfield || {};

Drupal.behaviors.cuepointfield = function () {
  $('.fp-description-content').hide();
  $('.fp-description-link').click(function () {
    $('.fp-description-content').slideToggle();
    return false;
  });
}