<?php

/**
* Implementation of views_plugin_style
*
*/

class views_plugin_style_fplaylist extends views_plugin_style {
 
  /**
   * Set default options
   */
  function options(&$options) {
    $options['play_button'] = '';
    $options['position'] = 'right';
  }
 
  /**
   * Provide a form for setting options.
   *
   * @param array $form
   * @param array $form_state
   */
  function options_form(&$form, &$form_state) {
    $form['play_button'] = array(
      '#type' => 'textarea',
      '#title' => t('Play button HTML'),
      '#default_value' => $this->options['play_button'],
    );
    $form['position'] = array(
      '#type' => 'radios',
      '#title' => t('Position of the playlist'),
      '#options' => array('right' => t('To the right of the player'), 'left' => t('To the left of the player')),
      '#default_value' => $this->options['position'],
    );
  }

}