<?php
/**
* Implementation of hook_views_plugins
*/
function fplaylist_views_plugins() {
  return array(
    'style' => array(
      'fp_playlist' => array(
        'title' => t('Flowplayer playlist'),
        'theme' => 'views_fplaylist',
        'help' => t('Displays a Flowplayer playlist and player with support for cuepoints.'),
        'handler' => 'views_plugin_style_fplaylist',
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}