
Flowplayer Playlist
------------------------

Flowplayer Playlist integrates Flowplayer with View and adds support for
cuepoints to add html overlays, pre- mid- and post-roll inline videos, and
trigger virtually any other action via javascript. Basically, Flowplayer is
just another player until you integrate playlists and cuepoints, then it's
a powerhouse.

Dependencies
------------------------
Flowplayer has a few dependencies. Here they are with some explaination:

* LESS CSS Preporcessor - http://drupal.org/project/less

If you haven't used LESS yet, take a look at http://lesscss.org/. It adds
simple programming within a CSS file. The reason for the dependency is to
simplify the CSS file, and allow relationships between sizes, instead of
flat definitions. In essence, you just change the width of the player in
the LESS CSS file, and everything else changes to match

* jQuery Update v6.x-2x (with jQuery 1.3) -
http://drupal.org/project/jquery_update

This dependency is required for sevearl reasons. I use the .closest()
jQuery command, some features use the jQuery Colorbox plugin, and there may
be some dependency on the .live() feature.

* CCK - http://drupal.org/project/cck
* Views - http://drupal.org/project/views

* Optional - Colorbox - http://drupal.org/project/colorbox

If you want to use the 'over' position in cuepoints (more explaination in
the description found under a Cuepoint Field field), then colorbox will
need to be installed.


To install
----------------------

1. Add the Flowplayer Playlist module folder to your module directory
(typically sites/all/modules.
2. Enable it in /admin/build/modules.
3. Also, you may want to enable the Cuepoint Field as well (bundled in the
fplaylist module)


To create a playlist
----------------------

1. Create a view
2a. To create a playlist where clicking anywhere on a playlist item plays
the video, the first link in each item should be to the video file to be
played. This link will be removed by the playlist script, so the text of
the link does not matter.
2b. Alternately, if you would like a play button in the playlist, and the
rest of the playlist item content to be HTML that can include other links,
you will need to add an anchor tag with the file to be played as the href
attribute, and 'play' as the class. When clicked on, this link will play
the item. For example: <a href="movie.mp4" class="play">Click to play</a>.
3. Set the style of the view to 'Flowplayer playlist,' and provide any
settings you'd like. You can set the playlist to show on the left or right
of the player.
4. The default CSS works best with the mini-pager option
5. Flowplayer playlist works well with the AJAX reloading, so enabling it
is suggested.
6. Save the view and take a look



To theme a playlist
----------------------
1. To have full control over the theme, go to /admin/settings/fplayer and
check the box next to 'Disable CSS'.
2. Copy the fplayer.less file from the fplaylist module directory and
either paste it into another LESS file you're using, or add it via your
theme's .info file.
3. Edit away!

Alternately, you can just override elements of the css in your theme layer.


To use cuepoints
----------------------
Flowplayer cuepoints are awesome, they let you trigger actions at any point
during a video. We've added a CCK field type to make adding cuepoints
really easy.

1. Enable the Cuepoint Field module (found in the fplaylist module
directory)
2. Add a 'Cuepoints' field to a node type
3. In your playlist view, make sure that this field is being loaded as part
of the content to display in the playlist. The output will not be visibile
unless you look at the source code, where you will see div's and hidden
inputs with cuepoint settings.
4. Follow the instructions below the cuepoint input when adding cuepoints.
You will have many options, including adding an inline file or triggering
an html overlay.

To use global cuepoints (cuepoints that apply to all videos):

1. Go to /admin/settings/cuepointfield
2. Add cuepoints and cuepoint content you would like added on specific
pages, and specify the pages to add these to.

Maintainers
-----------
Chris Shattuck