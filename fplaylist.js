
var FPlaylist = FPlaylist || {};

Drupal.behaviors.fplaylist = function () {
  FPlaylist.renderPlaylist(); // Render flowplayer playlists created by views
}

  
/**
 * Renders a playlist from a view and adds cuepoints and inline clips
 */
FPlaylist.renderPlaylist = function () {
  var overlay_class = '';
  var playlist_array = new Array(); // This will get pushed into player definition
  $('.make-playlist').each(function () {
    var index = 0;
    var is_items = false;
    var has_play = false;
    $(this).parents('.view').addClass('contains-playlist'); // Add class to outer view so we can do some themeing
    if ($(this).parents('.view').find('.pager').length > 0) { // Adding paging class for styling
      $(this).parents('.view').addClass('has-pager');
    } else {
      $(this).parents('.view').addClass('no-pager');
    }
    ($(this).find('.play-button').html()) ? play = $('.play-button').html() : play = '<div class="play_button"></div>'; // We need a play button for this playlist to work properly. Otherwise, for some reason the player is loaded but won't play a video if you click 'play'.
    $(this).parents('.view-content').find('li').each(function () { // Run through each li element
      $this_li = $(this);
      $(this).html($(this).html().replace("'",'(apostrophe)')); // Add apostrophe placeholder due to bug in playlist plugin
      $(this).find('.fp-cuepoints').find('.fp-cuepoint').append('<input type="hidden" class="index" value="' + index + '" ></index>'); // Add index so we can easily reference for cuepoints
      is_items = true;
      if ($this_li.find('.play').length > 0) { // There's a play button in each item
        has_play = true;
        item_url = $this_li.find('.play').attr('href');
      } else { // Everything will be the play button
        item_url = $(this).find('a:first').attr('href');
        $this_li.find('a:first').parents('[class*="views-field-"]').remove(); // Don't display in playlist
        $this_li.find('a').each(function () { // Replace all a tags with divs if a whole playlist item is a link, otherwise links mess up things.
          $(this).wrap('<div></div>').parent().html($(this).html());
        });
      }
      playlist_array.push({url:item_url, title: $(this).html()}); // Add playlist item
      index++;
    }).parents('.view-content').hide().find('.fp-cuepoints').remove(); // Hide view after pulling out content
    // Add left class if on left
    $(this).addClass('right').removeClass('left');
    var position = ' right';
    if ($(this).hasClass('left')) {
      position = ' left';
    }
    player = '<div class="views-player-overlay"></div><div id="views_player">' + play + '</div>';
    playlist_div = '<div class="views-playlist"><div class="playlist-item"><a class="play" href="${url}">${title}</a></div></div>'; 
    if (has_play) {
      playlist_div = '<div class="views-playlist"><div class="playlist-item">${title}</div></div>';
    }
    content = ($(this).hasClass('left')   ? playlist_div + player : player + playlist_div); // If you add the 'left' class to the view, it will add the playlist first, then the player.
    content += '<div style="clear:both"></div>'; // Clear the float
    $(this).parents('.view-content').after('<div class="fp-outer' + position + '">' + content + '</div>'); // Add content to view
    $(this).find('.play-button').hide(); // Hide the play button after moving the content over
    if (!is_items) { // Don't want to run flowplayer stuff if there's no items returned
      return;
    }
    
    
    // Create player and add analytics !TODO: Not sure analytics is working yet
    $f("views_player", '/' + Drupal.settings.flowplayerSwf,{
      clip: {
        scaling: 'fit',
        onStart: function(clip) {
          _tracker._trackEvent("Videos", "Play", clip.url);
        },
        onPause: function(clip) {
          _tracker._trackEvent("Videos", "Pause", clip.url, parseInt(this.getTime()));
        },
        onStop: function(clip) {
          _tracker._trackEvent("Videos", "Stop", clip.url, parseInt(this.getTime()));
        },
        onFullscreen: function(clip) {
          _tracker._trackEvent("Videos", "Fullscreen", clip.url, parseInt(this.getTime()));
        },
        onFullscreenExit: function(clip) {
          _tracker._trackEvent("Videos", "Fullscreenexit", clip.url, parseInt(this.getTime()));
        },
        onFinish: function(clip) {
          _tracker._trackEvent("Videos", "Finish", clip.url);
        }
      },
      playlist: playlist_array,
      plugins: {
        controls:{autoHide: 'always'}
      }
    });
    $f("views_player").playlist(".views-playlist", {loop:true});
    $(".views-playlist").find('a').each(function () {
      $(this).html($(this).html().replace("(apostrophe)","'")); // Replace out apostrophe placeholder
    });
    

    // Add cuepoints from the output of a cuepoint field
    $f().onBegin(function (clip) {
      var slide_timeout = ''; // Needs to be up scope a bit
      $('.fp-cuepoint:not(.fp-cuepoint-processed)').each(function () {
        var $this = $(this); // For easy reference later
        var length = $this.find('.length').val() * 1000;
        $f().onCuepoint($this.find('.start').val() * 1000, function (clip, cuepoint) { // Start cuepoint add
          if (clip.isInStream) { // Don't add cuepoints to instream clips
            return;
          }
          if (clip.index == $this.find('.index').val()) { // If this is the right video
            if ($this.find('.js').length > 0) { // If there's a javascript callback
              eval($this.find('.js').val() + "($f().getClip());");
            } else if ($this.find('.class').length > 0) { // If it's a class type cuepoint
              $('.views-player-overlay').slideUp('fast', function () { // If showing, hide first
                $('.views-player-overlay').removeClass(overlay_class);
                window.clearTimeout(slide_timeout); // Reset timeout to slide up overlay
                var content_class = '.' + $this.find('.class').val();
                var $item = $this.parents('.fp-cuepoints').find(content_class); // Grab content from within cuepoint data
                if ($item.length < 1) { // If it's empty, look to whole page
                  $item = $(content_class);
                }
                html = $item.html();
                overlay_class = $this.find('.overlay_class').val();
                if ($this.find('.position').val() == 'over') {
                  $.fn.colorbox({inline:true,href:content_class}); // Show colorbox with content
                  $('#cboxContent').addClass($this.find('.overlay_class').val());
                } else if ($this.find('.position').val() == 'top') {
                  $('.views-player-overlay').html('<div class="overlay-close"></div><div class="views-player-overlay-inner">' + html + '</div>'); // Pull in html
                  $('.overlay-close').click(function () {
                    $('.views-player-overlay').slideUp();
                  });
                  $('.views-player-overlay').addClass(overlay_class);
                  $('.views-player-overlay').slideDown(); // Show
                  slide_timeout = window.setTimeout(function () {$('.views-player-overlay').slideUp('fast', function () {$('.views-player-overlay').removeClass(overlay_class);}); }, length); // Hide later and remove overlay class
                }
              });
            }
          }
        });
        $this.addClass('fp-cuepoint-processed');
      });
    });
    
    // Add inline clips
    var i = 0;
    $f().onLoad(function () {
      $('.views-playlist').find('.fp-cuepoints:not(.fp-cuepoints-url-processed)').each(function () {
        $(this).find('.fp-cuepoint').each(function () {
          if ($(this).find('.url').length > 0) {
            length = ($(this).find('.length').length > 0) ? $(this).find('.length').val() : -1;
            $f().addClip({url: $(this).find('.url').val(), position: $(this).find('.start').val(), duration: length}, i);
          }
        });
        $(this).addClass('fp-cuepoints-url-processed');
        i++;
      });
    });

   
  });
}