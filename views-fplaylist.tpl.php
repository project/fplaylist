<?php
/**
 * @file views-view-list.tpl.php
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<div class="item-list">
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <div class="make-playlist <?php if ($options['position'] == 'left') { echo 'left'; } else { echo 'right'; }?>">
    <?php if ($options['play_button']) { ?>
      <div class="play-button"><?php print $options['play_button'];?></div>
    <?php } ?>
    <ul>
      <?php foreach ($rows as $id => $row): ?>
        <li class="<?php print $classes[$id]; ?>"><?php print $row; ?></li>
      <?php endforeach; ?>
    </ul>
  </div>
</div>